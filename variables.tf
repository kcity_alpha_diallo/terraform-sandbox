variable "vpc_id" {
  type = string
  default = "vpc-07800986a79692ff8"
}

variable "subnet_id1_alb" {
  type = string
  default = "subnet-028e032cb321c08ab"
}

variable "subnet_id2_alb" {
  type = string
  default = "subnet-0d380b61b604bfaae"
}

variable "cluster_name" {
  type = string
  default = "cluster-alpha"  
}

variable "repo_image" {
  type = string
  default = "866164239950.dkr.ecr.us-east-1.amazonaws.com/ref1:latest"
}

variable "security_group" {
  type = string
  default = "sg-01ed106d076e584a0"
}


variable "deregistration_delay" {
  default = "60"
}

variable "cookie_duration" {
  default = 1800
}

variable "health_check_path" {
  default = "/"
}

variable "health_check_port" {
  default = 8000
}

variable "health_check_healthy_threshold" {
  default = 6
}

variable "health_check_unhealthy_threshold" {
  default = 2
}

variable "health_check_timeout" {
  default = 10
}

variable "health_check_interval" {
  default = 30
}

variable "health_check_matcher" {
  default = "200"
}

variable "tg_protocol" {
  default = "HTTP"
}

variable "tg_port" {
  default = 80
}

variable "tg_stickiness_type" {
  default = "lb_cookie"
}

variable "lb_access_logs_expiration_days" {
  default = "30"
}

variable "url" {
  type = string
  default = "alpha.kinaxia.fr"
}

variable "certificat" {
  default = "arn:aws:acm:us-east-1:866164239950:certificate/fc6a6e26-c074-4446-b85e-9117b0c4b90e"
}


variable "scale_cpu_size" {
  default = 50
}

variable "scale_memory_size" {
  default = 50
}

variable "retention_in_days" {
   default = 30
}