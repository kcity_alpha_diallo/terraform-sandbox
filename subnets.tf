resource "aws_subnet" "subnet1" {
  vpc_id     = var.vpc_id
  cidr_block = "10.0.2.0/24"
  availability_zone= "us-east-1c"

  tags = {
    Name = "subnet1_containers"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id     = var.vpc_id
  cidr_block = "10.0.3.0/24"
  availability_zone= "us-east-1d"

  tags = {
    Name = "subnet2_containers"
  }
}

# Attach nat_gat to subnets
resource "aws_route_table_association" "nat_subnet1" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = "rtb-02f6cf32a1070c37d"
}

resource "aws_route_table_association" "nat_subnet2" {
  subnet_id      = aws_subnet.subnet2.id
  route_table_id = "rtb-02f6cf32a1070c37d"
}
