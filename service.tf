resource "aws_ecs_service" "ecs_service" {
  name            = "service_fargate"
  cluster         = var.cluster_name
  task_definition = aws_ecs_task_definition.task_def.arn
  desired_count   = 2
  scheduling_strategy = "REPLICA"
  health_check_grace_period_seconds = 60
  launch_type = "FARGATE"
  
  lifecycle {
    ignore_changes = [desired_count, task_definition]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn
    container_name   = "nginx_server"
    container_port   = 80
  }

  network_configuration {
    subnets = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]
    security_groups = [aws_security_group.allow_acces_alb_to_containers.id]
    assign_public_ip = false
  } 
}