resource "aws_cloudwatch_log_group" "logs_containers" {
  name = "cloudwatchlog_test"
  retention_in_days = var.retention_in_days
}