resource "aws_lb_target_group" "target_group" {
  name        = "targetg"
  target_type = "ip"
  port        = var.tg_port
  protocol    = var.tg_protocol
  vpc_id      = var.vpc_id
  deregistration_delay = var.deregistration_delay  

  health_check {
    path                = var.health_check_path
    port                = var.health_check_port
    healthy_threshold   = var.health_check_healthy_threshold
    unhealthy_threshold = var.health_check_unhealthy_threshold
    timeout             = var.health_check_timeout
    interval            = var.health_check_interval
    matcher             = var.health_check_matcher  # has to be HTTP 200 or fails
  }

  stickiness {    
    type            = var.tg_stickiness_type    
    cookie_duration = var.cookie_duration   
    enabled         = true  
  }
}
