resource "aws_lb" "alb" {
  name               = "albtest"
  load_balancer_type = "application"
  security_groups    = [var.security_group]
  subnets            = [var.subnet_id1_alb, var.subnet_id2_alb]
}


resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = var.certificat

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Access denied to this url"
      status_code  = "400"
    }
  }
  
}


resource "aws_lb_listener_rule" "redirect_tg" {
  listener_arn = aws_lb_listener.listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }

  condition {
    path_pattern {
      values = [var.url]
    }
  }

}