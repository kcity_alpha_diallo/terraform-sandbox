data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "role-ecs"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}




resource "aws_ecs_task_definition" "task_def" {
  family = "task"
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"   # mandatory if requires_compatibilities is define to FARGATE
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
  cpu = 256
  memory = 512
  container_definitions = jsonencode([
    {
      name      = "nginx_server"
      image     = var.repo_image
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          # hostPort      = 80  : must be nothing or same value with containerport because of awsvpc mode
        }
      ]
      logConfiguration = {
        logDriver = "awslogs"
        secretOptions = null
        options = {
          awslogs-group = aws_cloudwatch_log_group.logs_containers.name
          awslogs-region = "us-east-1"
          awslogs-stream-prefix = "ecs"
        }
      } 
    }
    ])
}
