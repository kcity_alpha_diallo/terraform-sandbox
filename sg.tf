resource "aws_security_group" "allow_acces_alb_to_containers" {
  name        = "sgroup"
  description = "Allow HTTP inbound traffic"
  vpc_id      = var.vpc_id
  ingress = [
    {
      description      = "HTTPS from subnet alb"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["10.0.0.0/24","10.0.1.0/24"]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
    }
  ]

  egress = [
    {
      description      = "Allows all trafic to internet"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
    }
  ]

  tags = {
    Name = "allow_alb_access_to_containers"
  }
}